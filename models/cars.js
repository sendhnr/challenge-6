'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Cars extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Cars.init({
    name: DataTypes.STRING,
    price: DataTypes.STRING,
    size: DataTypes.STRING,
    images: DataTypes.ARRAY,
    role: DataTypes.ENUM
  }, {
    sequelize,
    modelName: 'Cars',
  });
  Product.associate = function (models) {
    //association can be defined here
    Product.belongsTo(models.Warehouse, {
      foreignKey: 'warehouseId'
    })
  }

  return Cars;
};