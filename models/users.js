'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Users extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Users.init({
    name: DataTypes.STRING,
    email: {
      type: DataTypes.STRING,
      validate: {
        isEmail: true
      }
    },
    password: {
      type: DataTypes.STRING,
      validate: {
        customValidator(value) {
          if (value.length <= 8)
            throw new ApiError(httpStatus.BAD_REQUEST, "password gak boleh kurang dari 8 karakter")
        }
      }
    }
  }, {
    sequelize,
    modelName: 'Users',
  });
  return Users;
};