const { Cars } = require('../models/cars')
const imageKit = require('../lib/imageKit')
const { Op } = require('sequelize')

const createCars = async (req, res) => {
    const { name, price, size, image } = req.body
    const file = req.file
    console.log(req.user)
    try {
        // validasi utk format file image
        // const validFormat = file.mimetype == 'image/png' || file.mimetype == 'image/jpg' || file.mimetype == 'image/jpeg' || file.mimetype == 'image/gif';
        // if (!validFormat) {
        //     return res.status(400).json({
        //         status: 'failed',
        //         message: 'Wrong Image Format'
        //     })
        // }

        // // untuk dapat extension file nya
        // const split = file.originalname.split('.')
        // const ext = split[split.length - 1]

        // // upload file ke imagekit
        // const img = await imageKit.upload({
        //     file: file.buffer, //required
        //     fileName: `IMG-${Date.now()}.${ext}`, //required
        // })

        // validasi user yg input punya warehouse nya gak
        // if (req.user.warehouseId != warehouseId) {
        //     return res.status(401).json({
        //         status: 'fail',
        //         message: 'You are not owner of this warehouse'
        //     })
        // }

        const newCars = await Cars.create({
            name,
            price,
            size,
            //warehouseId: req.user.warehouseId,
            // image: img.url,
        })

        res.status(201).json({
            status: 'success',
            data: {
                newCars
            }
        })
    } catch (err) {
        res.status(404).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

module.exports = {
    createCars
}