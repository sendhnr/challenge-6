require('dotenv').config();
const express = require('express');
const morgan = require('morgan')

const cors = require('cors')
const httpStatus = require('http-status')

//import routing
const router = require('./routes/cars')
//port
const port = 3000
//initialization after statement
const app = express()
//basic express configuration
app.locals.moment = require('moment')

// Middleware to Parse JSON
app.use(express.json())
app.use(cors())
// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended:true }))

app.use(morgan('dev'))
app.use(router);

//running port
app.listen(port, () => {
    console.log(`Server running on ${Date(Date.now)}`)
    console.log(`Server listening on PORT: ${port}`)
})