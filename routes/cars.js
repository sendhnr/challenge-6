const router = require('express').Router()

//import from controller
const carsController = require('../controllers/carsController')

router.post('/api/cars', carsController.createCars)

module.exports = router